# Use https://googleapis.dev/python/pubsub/latest/index.html as a reference to understand
# how to use the Python API client. Another option would be to directly consume the pubsub
# REST or gRPC API documented here: https://cloud.google.com/pubsub/docs/reference/service_apis_overview

import os
from google.cloud import pubsub_v1


# TODO: Change your default settings accordingly or set them in your shell and export them
# Here, we're loading the vars from the shell environment running the Python
project_id = os.getenv('PROJECT_ID','javiercm-main-demos')
topic_id = os.getenv('TOPIC_ID','my-topic')


# We first instantiate a publishing client to be able to publish to Pub/Sub
# https://googleapis.dev/python/pubsub/latest/publisher/api/client.html
publisher = pubsub_v1.PublisherClient()
# The `topic_path` method creates a fully qualified identifier
# in the form `projects/{project_id}/topics/{topic_id}` that is needed to
# address our specific topic univocally.
topic_path = publisher.topic_path(project_id, topic_id)


for n in range(1, 10):
    # Let's compose a simple data string using Python 3's f-strings
    # https://docs.python.org/3/tutorial/inputoutput.html
    data = f"Message number {n}"
    # Data must be a bytestring, so let's encode it
    # https://docs.python.org/3/library/stdtypes.html#str.encode
    data = data.encode("utf-8")
    # When you publish a message, the client returns a future:
    # https://googleapis.dev/python/pubsub/latest/publisher/api/futures.html
    future = publisher.publish(topic_path, data)
    # Finally, we resolv the the future by returning the meessage ID:
    print(future.result())

print(f"Published messages to {topic_path}.")