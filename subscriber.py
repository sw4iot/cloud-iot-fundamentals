# https://docs.python.org/3/library/concurrent.futures.html?highlight=futures%20future#concurrent.futures.TimeoutError
from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import os

# TODO: Change your default settings accordingly or set them in your shell and export them
# Here, we're loading the vars from the shell environment running the Python
project_id = os.getenv('PROJECT_ID','javiercm-main-demos')
subscription_id = os.getenv('SUB_ID','my-sub')
timeout = 5.0

# We first instantiate a subscriber client to be able to pull from Pub/Sub
# https://googleapis.dev/python/pubsub/latest/subscriber/api/client.html
subscriber = pubsub_v1.SubscriberClient()
# The `subscription_path` method creates a fully qualified identifier
# in the form `projects/{project_id}/subscriptions/{subscription_id}` that is needed to
# address our specific topic univocally.
subscription_path = subscriber.subscription_path(project_id, subscription_id)

# We're going to do an asynchronous pull of messages from the subscription
# https://googleapis.dev/python/pubsub/latest/subscriber/index.html#pulling-a-subscription-asynchronously
# In this case, we need to define a callback function to process an incoming message
def callback(message):
    print(f"Received {message}.")
    # We tell Pub/Sub that the message has been received and shouldn't be redelivered
    # Typically a bit more checking is recommended before ACKíng
    message.ack()

# This starts a background thread to begin pulling messages from the subscription
# asynchronously and scheduling them to be processed using the provided callback, that
# will be called with with an individual google.cloud.pubsub_v1.subscriber.message.Message
# The callback is resposible for ACKing or NACKing on the message when finished.
streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
print(f"Listening for messages on {subscription_path}..\n")

# Wrap subscriber in a 'with' block to automatically call close() when done.
with subscriber:
    try:
        # When `timeout` is not set, result() will block indefinitely,
        # unless an exception is encountered first.
        streaming_pull_future.result(timeout=timeout)
    except TimeoutError:
        streaming_pull_future.cancel()